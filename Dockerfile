FROM openjdk:8-jdk-alpine
ARG JAR_FILE
COPY ${JAR_FILE} myapp.jar
ENTRYPOINT ["java","-jar","/myapp.jar"]